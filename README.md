# Real-time social media sentiment analysis

[RajasthanSentimentApp](https://rajasthansentiment.herokuapp.com)

![dashboard](images/dashboard.jpg)
![nav_menu](images/nav_menu.jpg)

#Team Name: fsociety
###Problem Category: Machine Learning
*About RajasthanSentimentApp*

The Government of Rajasthan uses social media (twitter, facebook) to engage with its citizens. In response, the citizens share their sentiments. We're developing an application that'll analyze citizen's tweets (using Twitter streaming API) and intelligently provide real-time feedback using sentiment analysis (natural language processing). The application will provide a real-time statistical average sentiment score (high meaning positive feedback). The app will also present the summary(history) of citizen's sentiments about a topic in a visual manner with the help of time series graph.

The application will follow a client-server architecture consisting of two key components:

    1. An android app
    2. A web-server (Django) 
    3. Natural Language Processing (integrated in the server)


####Natural Language Processing
We use NLTK(with its corpras) to train for tweets and Textblob for real time analysis.
The Stream of tweets obtained from the twitter Streaming API are provided to the textblob classifiers on the web server.
The web server passes on the sentiment and other analysis to the android app via a REST API.

####Django Server
<https://rajasthansentiment.herokuapp.com/>

The server acts as an intermediaryfor the app and the ML model. The Model analysis is done on the server.
it passes on the processed information to the app. 

####Android App
The Sentiment analysis data is extracted from the API and visualized via a pie chart.
       
![real_time_analysis](images/real_time_analysis.jpg)
![sentimentpage](images/sentimentpage.jpg)
![last_week_activity](images/last_week_activity.jpg)
![analyze_text](images/analyze_text.jpg)

